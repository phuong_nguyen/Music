package com.example.phuongnguyenthi1.music.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.fragment.SongsFragment;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.Holder> {

    Context mContext;
    SongsFragment mSongFrag;
    ArrayList<Song> mListSongs = new ArrayList();
    OnClickItemSongListener mListener;
    public interface OnClickItemSongListener{
        public void onItemSongSelect(int position);
    }

    public SongAdapter(){

    }

    public SongAdapter(Context mContext, ArrayList<Song> mListSongs, OnClickItemSongListener onClickItemSongListener) {

        this.mContext = mContext;
        this.mListSongs = mListSongs;
        this.mListener = onClickItemSongListener;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_song, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        Song song = mListSongs.get(position);
        holder.mTvTitleSong.setText(song.getmTitle());
        holder.mTvArtist.setText(song.getmArtist());
        Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri,
                (long)song.getID());
//        Glide.with(mSongFrag).load(uri).(R.drawable.default_song).error(R.drawable.default_song)
//                .crossFade().centerCrop().into(holder.mImgSong);

        RequestOptions options = new RequestOptions()
                .error(R.drawable.default_song)
                .placeholder(R.drawable.default_song);
        Glide.with(mContext).load(uri).apply(options).into(holder.mImgSong);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( view != null && mListSongs .size() != 0){
                    mListener.onItemSongSelect(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListSongs.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView mImgSong;
        TextView mTvTitleSong, mTvArtist;
        public Holder(View itemView) {
            super(itemView);

            mImgSong = itemView.findViewById(R.id.rcv_item_song_img_song);
            mTvTitleSong = itemView.findViewById(R.id.rcv_item_song_tv_title_song);
            mTvArtist = itemView.findViewById(R.id.rcv_item_song_tv_artist_song);
        }
    }
}
