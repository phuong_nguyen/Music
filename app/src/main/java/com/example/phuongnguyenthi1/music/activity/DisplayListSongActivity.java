package com.example.phuongnguyenthi1.music.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.phuongnguyenthi1.music.MainActivity;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.adapter.SongAdapter;
import com.example.phuongnguyenthi1.music.fragment.AlbumsFragment;
import com.example.phuongnguyenthi1.music.fragment.ArtistsFragment;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

import static com.example.phuongnguyenthi1.music.fragment.AlbumsFragment.LIST_SONG_OF_ALBUM;
import static com.example.phuongnguyenthi1.music.fragment.SongsFragment.PATH_SONG;

public class DisplayListSongActivity extends AppCompatActivity implements SongAdapter.OnClickItemSongListener{


    RecyclerView mRcvListSong;
    SongAdapter mSongAdapter;
    ArrayList<Song> mListSong = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_list_song);

        mRcvListSong = findViewById(R.id.acti_display_list_song_rcv_song);
        String nameFrag = getIntent().getStringExtra(AlbumsFragment.NAME_FRAGMENT);
        if( nameFrag.equals(AlbumsFragment.ALBUM_FRAGMENT)){
            mListSong = (ArrayList<Song>) getIntent().getSerializableExtra(LIST_SONG_OF_ALBUM);

        }else {
            mListSong = (ArrayList<Song>) getIntent().getSerializableExtra(ArtistsFragment.LIST_SONG_OF_ARTIST);

        }
        LinearLayoutManager layout = new LinearLayoutManager(getApplication());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvListSong.setLayoutManager(layout);
        mSongAdapter = new SongAdapter(this.getApplicationContext(), mListSong, this);
        mRcvListSong.setAdapter(mSongAdapter);
    }

    @Override
    public void onItemSongSelect(int position) {
        Intent intent = new Intent(this, PlayMusicActivity.class);
        intent.putExtra(PlayMusicActivity.LISTSONG_IN_PLAYMUSIC, mListSong);
        intent.putExtra(PlayMusicActivity.POSITION_PLAY, position);
        startActivity(intent);

    }
}
