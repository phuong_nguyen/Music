package com.example.phuongnguyenthi1.music.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.R;

public class BackgroundService extends Service {
    public static final String TAG= null;
    public static MediaPlayer mPlayer;
    static int  mTime =100;
    static int mIndex;
    private IBinder mBinder = new MyBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        BackgroundService getService() {
            return BackgroundService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPlayer = MediaPlayer.create(this, R.raw.somethings);
        mPlayer.setLooping(true);
        mPlayer.setVolume(100, 100);
        mTime = mPlayer.getDuration();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mPlayer.start();
        return START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {

    }

    @Override
    public void onDestroy() {
        mPlayer.stop();
        mPlayer.release();
    }

    public   static void onPauseMusic(){
        if( mPlayer.isPlaying()){
            mIndex = mPlayer.getCurrentPosition();
            mPlayer.pause();
        }else {
//            mPlayer.seekTo(mIndex);
            mPlayer.start();
        }

    }
    public static int getCurrentPos(){
        mIndex = mPlayer.getCurrentPosition();
        return mIndex;
    }
    public static void  setCurrentPos(int pos){
        mIndex = pos;
    }
    public static void update(int time){
        mIndex = time;
        mPlayer.seekTo(mIndex);
        mPlayer.start();
    }

    public static  int getMaxTime(){
        return mTime;
    }

    private void playPause() {
        //
    }

    private void seekTo(int position) {

    }

    private void next(int position) {

    }

    private void back() {

    }

    private long getDuration() {
        return 0;
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("PLAY_PAUSE".equals(action)) {
                playPause();
            }
        }
    };
}





