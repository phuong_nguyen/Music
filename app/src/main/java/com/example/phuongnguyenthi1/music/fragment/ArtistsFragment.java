package com.example.phuongnguyenthi1.music.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.MainActivity;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.activity.DisplayListSongActivity;
import com.example.phuongnguyenthi1.music.adapter.ArtistAdapter;
import com.example.phuongnguyenthi1.music.model.Album;
import com.example.phuongnguyenthi1.music.model.Artist;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

public class ArtistsFragment extends Fragment implements ArtistAdapter.OnClickItemArtistListener {

    public static final String LIST_SONG_OF_ARTIST = "LIST_SONG_OF_ARTIST";
    public static final String ARTIST_FRAGMENT = "ARTIST_FRAGMENT";
    RecyclerView mRcvArtist;
    ArtistAdapter mArtistAdapter ;
    ArrayList<Song> mListSong = new ArrayList<>();
    ArrayList<Artist> mListArtist = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artists, container, false);

//        mListSong = (ArrayList<Song>) getArguments().getSerializable(MainActivity.LIST_SONG);
//        initListArtist();
        mListArtist = getlistArtist();

        mRcvArtist = view.findViewById(R.id.fr_album_rcv_artist);
        mArtistAdapter = new ArtistAdapter(this.getContext(),  mListArtist, this);
        GridLayoutManager mLayout = new GridLayoutManager(this.getContext(), 2);
        mRcvArtist.setLayoutManager(mLayout);
        mRcvArtist.setAdapter(mArtistAdapter);
        return view;
    }


    private void initListArtist() {
        for( int i =0; i < mListSong.size(); i++){
            Song song = mListSong.get(i);
            int position = getPositionInList(song.getmArtist());
            if( position == -1){
                Artist artist = new Artist(song.getmArtist());
                artist.addNewSong(song);
                mListArtist.add(artist);
            }else {
                mListArtist.get(position).addNewSong(song);
            }
        }
    }

    private int getPositionInList(String s) {
        for( int i=0; i< mListArtist.size(); i++){
            if( s.equals(mListArtist.get(i).getName())){
                return i;
            }
        }
        return -1;
    }

    public ArrayList<Artist> getlistArtist(){
        ArrayList<Artist> listArtist = new ArrayList<>();
        Cursor cursor = getActivity().getContentResolver().query(
                MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                new String[] {
                        MediaStore.Audio.Artists.ARTIST,
                        MediaStore.Audio.Artists._ID}, null, null,
                null);
        if (cursor != null) {
            Log.d("TAG", "artist:  " + cursor.getCount());
            cursor.moveToFirst();
            do{
                String artist = cursor.getString(0);
                Log.e("artist",artist);
                String id = cursor.getString(1);
                Artist temp = new Artist(1, artist, id);
                listArtist.add(temp);
            }while(cursor.moveToNext());
        }

//        String[] proj = {MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST, MediaStore.Audio.Artists.NUMBER_OF_ALBUMS, MediaStore.Audio.Artists.NUMBER_OF_TRACKS };
//        musiccursor = managedQuery(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, proj, null, null, MediaStore.Audio.Artists.ARTIST + " ASC");
//        String[] from= new String[]{ MediaStore.Audio.Artists.ARTIST, MediaStore.Audio.Artists.NUMBER_OF_ALBUMS, MediaStore.Audio.Artists.NUMBER_OF_TRACKS };
        return listArtist;
    }


    @Override
    public void onItemArtistSelect(int position) {
        ArrayList<Song> listSong = new ArrayList<>();
        listSong = mListArtist.get(position).getmListSong();
        Intent intent = new Intent(this.getActivity(), DisplayListSongActivity.class);
        intent.putExtra(AlbumsFragment.NAME_FRAGMENT, ARTIST_FRAGMENT);
        intent.putExtra(LIST_SONG_OF_ARTIST, listSong);
        startActivity(intent);
    }
}
