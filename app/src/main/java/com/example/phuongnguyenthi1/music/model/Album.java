package com.example.phuongnguyenthi1.music.model;

import android.content.ContentUris;
import android.net.Uri;

import java.util.ArrayList;

public class Album {
    int mNumberSong;
    String mAlbumId, mName;
    String mPathUri;
    ArrayList<Song> mListSong = new ArrayList<>();

    public Album() {
    }

    public Album(String mAlbumId, String mName, int mNumberSong) {
        this.mAlbumId = mAlbumId;
        this.mName = mName;
        this.mNumberSong = mNumberSong;
        Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri,
                Long.parseLong(this.mAlbumId));
    }

    public Album(String mAlbumId, String mName, ArrayList<Song> mListSong) {
        this.mAlbumId = mAlbumId;
        this.mName = mName;
        this.mListSong = mListSong;
    }

    public String getmAlbumId() {
        return mAlbumId;
    }

    public void setmAlbumId(String mAlbumId) {
        this.mAlbumId = mAlbumId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public ArrayList<Song> getmListSong() {
        return mListSong;
    }

    public void setmListSong(ArrayList<Song> mListSong) {
        this.mListSong = mListSong;
    }
    public void addNewSong(Song song){
        mListSong.add(song);
    }

    public int getmNumberSong() {
        return mNumberSong;
    }

    public void setmNumberSong(int mNumberSong) {
        this.mNumberSong = mNumberSong;
    }

    public String getmPathUri() {
        return mPathUri;
    }
}
