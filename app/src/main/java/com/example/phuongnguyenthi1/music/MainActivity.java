package com.example.phuongnguyenthi1.music;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.activity.PlayMusicActivity;
import com.example.phuongnguyenthi1.music.adapter.PagerAdapter;
import com.example.phuongnguyenthi1.music.fragment.AlbumsFragment;
import com.example.phuongnguyenthi1.music.fragment.ArtistsFragment;
import com.example.phuongnguyenthi1.music.fragment.SongsFragment;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String LIST_SONG  = "LIST_SONG";
    TabLayout mTablayout;
    ViewPager mViewPager;
    PagerAdapter mPagerAdaper;
    ArrayList<Fragment> mListFragment = new ArrayList<>();
    ArrayList<String> mListTitle = new ArrayList<>();

    ArrayList<Song> mListSong = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        checkPermission();
        mListSong =  getListSong();

        innitListFrag();
        innitListTitle();
        innitViewPager();
        initTablayout();

    }

    private ArrayList<Song> getListSong() {

        ArrayList<Song> listSong = new ArrayList<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String [] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID
        };
        Cursor cursor = getContentResolver().query(uri, projection, MediaStore.Audio.Media.DATA + " like ?",
                new String []{"%mp3"}, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do{
//                        String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
//                        String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
//                        String url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
//                        String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
//
                    int id = Integer.parseInt(cursor.getString(0));
                    String data = cursor.getString(1);
                    String displayName = cursor.getString(2);
                    String artist = cursor.getString(3);
                    String album = cursor.getString(4);
                    String albumId = cursor.getString(5);
                    Song song = new Song(id, artist, displayName, data, album, albumId);
                    listSong.add(song);

                }while (cursor.moveToNext());
            }


            cursor.close();
        }
        return listSong;
    }

    private void innitViewPager() {
        mViewPager =findViewById(R.id.view_pager);
        mPagerAdaper = new PagerAdapter(this.getSupportFragmentManager(), this.getApplicationContext(), mListFragment, mListTitle);
        mViewPager.setAdapter(mPagerAdaper);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void innitListTitle() {
        mListTitle.add("Songs");
        mListTitle.add("Artists");
        mListTitle.add("Albums");
    }

    private void innitListFrag() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(LIST_SONG, mListSong);

        SongsFragment mSongFragment = new SongsFragment();
        mSongFragment.setArguments(bundle);
        mListFragment.add(mSongFragment);

        ArtistsFragment mArtistFragment = new ArtistsFragment();
        mArtistFragment.setArguments(bundle);
        mListFragment.add(mArtistFragment);


        mListFragment.add(new AlbumsFragment());
    }

    public void initTablayout(){
        mTablayout = findViewById(R.id.tab_layout);
        mTablayout.setupWithViewPager(mViewPager);
        mTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                mViewPager.setCurrentItem(tab.getPosition());
                mTablayout.getTabAt(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void checkPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if( checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }
}
