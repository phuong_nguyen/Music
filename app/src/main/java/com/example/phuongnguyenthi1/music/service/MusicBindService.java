package com.example.phuongnguyenthi1.music.service;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.phuongnguyenthi1.music.R;

import java.io.IOException;

public class MusicBindService extends Service {
    public static final String ACTION_PLAY_PAUSE = "com.example.phuongnguyenthi1.music.PLAY_PAUSE";
    public static final String ACTION_SEEK_TO = "com.example.phuongnguyenthi1.music.SEEK_TO";
    public static final String ACTION_GET_DURATION = "com.example.phuongnguyenthi1.music.GET_DURATION";
    public static final String ACTION_GET_POSTION = "com.example.phuongnguyenthi1.music.GET_POSITION";
    public static final String DURATION = "DURATION";
    MediaPlayer mPlayMusic;
    String mPathSong;
    private final IBinder mBinder = new MyBinder();

    public class MyBinder extends Binder {
        public MusicBindService getService(){
            return MusicBindService.this;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("TAG", "on bind");
        return mBinder;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("TAG", "on create");
        initMediaPlayer();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("TAG", "on start command");
        if (mPlayMusic != null) {
            mPlayMusic.start();
        }

        return START_STICKY;
    }

    public void setPathSong(String path){
        this.mPathSong = path;
    }

    public void initMediaPlayer(){
        if( mPlayMusic != null){
            mPlayMusic.release();
            mPlayMusic = null;
        }
        mPlayMusic = new MediaPlayer();
    }

    public void reset(){
        if( mPlayMusic != null){
            mPlayMusic.release();
            mPlayMusic = null;
        }
    }

    public void setupMediaPlayer(){
        try {
            if( !mPathSong.equals("")){
                mPlayMusic.setDataSource(mPathSong);
                mPlayMusic.setLooping(true);
                mPlayMusic.prepareAsync();
                mPlayMusic.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mediaPlayer.seekTo(0);
                        mediaPlayer.start();
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public void play() {
        if( mPlayMusic != null){
            mPlayMusic.start();
        }
    }

    public void pause() {
        if( mPlayMusic != null && isPlaying()){
            mPlayMusic.pause();
        }
    }

    public boolean isPlaying() {
        return mPlayMusic!= null && mPlayMusic.isPlaying();
    }

    public  void seekTo(int position) {
        if (mPlayMusic != null) {
            mPlayMusic.seekTo(position);
            mPlayMusic.start();
        }
    }

    public int getCurrentPosition(){
        if( mPlayMusic != null){
            return mPlayMusic.getCurrentPosition();
        }
        return 0;
    }

    private void next(int position) {

    }

    private void back() {

    }

    public long getDuration() {
        if( mPlayMusic != null)
            return mPlayMusic.getDuration();
        return 0;
    }

}
