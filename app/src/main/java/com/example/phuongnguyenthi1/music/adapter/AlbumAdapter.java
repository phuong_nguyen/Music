package com.example.phuongnguyenthi1.music.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.fragment.ArtistsFragment;
import com.example.phuongnguyenthi1.music.model.Album;
import com.example.phuongnguyenthi1.music.model.Artist;

import java.util.ArrayList;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.Holder> {

    private ArrayList<Album> mListAlbum = new ArrayList<>();
    private OnClickItemAlbumListener mListener;
    private Context mContext;
    public interface OnClickItemAlbumListener{
        public void onItemAlbumSelect( int position);
    }

    public AlbumAdapter(Context mContext, ArrayList<Album> mListAlbum, OnClickItemAlbumListener onClickItemAlbumListener) {
        this.mListener = onClickItemAlbumListener;
        this.mContext = mContext;
        this.mListAlbum = mListAlbum;
    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_album, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        Album album = mListAlbum.get(position);
        holder.mTvNameAlbum.setText(album.getmName());
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.default_album)
                .error(R.drawable.default_album);
        Glide.with(mContext).load(album.getmPathUri()).apply(options).into(holder.mImgAlbum);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( view != null && mListAlbum.size() != 0){
                    mListener.onItemAlbumSelect(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListAlbum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView mImgAlbum;
        TextView mTvNameAlbum;
        public Holder(View itemView) {
            super(itemView);
            mImgAlbum = itemView.findViewById(R.id.rcv_item_album_img_album);
            mTvNameAlbum = itemView.findViewById(R.id.rcv_item_album_tv_name_album);
        }
    }
}
