package com.example.phuongnguyenthi1.music.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.fragment.ArtistsFragment;
import com.example.phuongnguyenthi1.music.model.Artist;

import java.util.ArrayList;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.Holder> {

    private ArrayList<Artist> mListArtist = new ArrayList<>();
    private OnClickItemArtistListener mListener;
    Context mContext;
    public interface OnClickItemArtistListener{
        public void onItemArtistSelect( int position);
    }

    public ArtistAdapter(Context  mContext, ArrayList<Artist> mListArtist, OnClickItemArtistListener mListener) {
        this.mListener = mListener;
        this.mListArtist = mListArtist;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_artist, parent, false);

        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        Artist artist = mListArtist.get(position);

        holder.mTvNameArtist.setText(artist.getName());
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.default_album)
                .error(R.drawable.default_album);
        Glide.with(mContext).load(artist.getmPathUri()).apply(options).into(holder.mImgArtist);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( view != null){
                    mListener.onItemArtistSelect(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListArtist.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView mImgArtist;
        TextView mTvNameArtist;
        public Holder(View itemView) {
            super(itemView);
            mImgArtist = itemView.findViewById(R.id.rcv_item_artist_img_artist);
            mTvNameArtist = itemView.findViewById(R.id.rcv_item_artist_tv_name);
        }
    }
}
