package com.example.phuongnguyenthi1.music.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.MainActivity;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.activity.DisplayListSongActivity;
import com.example.phuongnguyenthi1.music.adapter.AlbumAdapter;
import com.example.phuongnguyenthi1.music.adapter.ArtistAdapter;
import com.example.phuongnguyenthi1.music.model.Album;
import com.example.phuongnguyenthi1.music.model.Artist;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

public class AlbumsFragment extends Fragment implements AlbumAdapter.OnClickItemAlbumListener {

    public static final String LIST_SONG_OF_ALBUM = "LIST_SONG_OF_ALBUM";
    public static final String NAME_FRAGMENT= "NAME_FRAGMENT";
    public static final String ALBUM_FRAGMENT= "ALBUM_FRAGMENT";
    RecyclerView mRcvAlbum;
    AlbumAdapter mAlbumAdapter;
    ArrayList<Song> mListSong = new ArrayList<>();
    ArrayList<Album> mListAlbum = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);


        mRcvAlbum = view.findViewById(R.id.fr_album_rcv_album);

        GridLayoutManager mLayout = new GridLayoutManager(this.getContext(), 2);
        mRcvAlbum.setLayoutManager(mLayout);

        mListAlbum = getListAlbum();
        mAlbumAdapter = new AlbumAdapter(this.getContext(),  mListAlbum, this);
        mRcvAlbum.setAdapter(mAlbumAdapter);

        getListSongOfAlbum();

        return view;
    }

    private void initListAlbum() {
        for( int i =0; i < mListSong.size(); i++){
            Song song = mListSong.get(i);
            int position = getPositionInList(song.getmAlbum());
            if( position == -1){
                Album album = new Album(song.getmAlbumId(), song.getmAlbum(), 1);
                album.addNewSong(song);
                mListAlbum.add(album);
            }else {
                mListAlbum.get(position).addNewSong(song);
            }
        }
    }

    private int getPositionInList(String s) {
        for( int i=0; i< mListAlbum.size(); i++){
            if( s.equals(mListAlbum.get(i).getmAlbumId())){
                return i;
            }
        }
        return -1;
    }
    public ArrayList<Album> getListAlbum(){
        ArrayList<Album> listAlbum = new ArrayList<>();
        Cursor cursor = getActivity().getContentResolver().query(
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[] {
                        MediaStore.Audio.Albums.ARTIST,
                        MediaStore.Audio.Albums._ID,
                        MediaStore.Audio.Albums.NUMBER_OF_SONGS,
                        MediaStore.Audio.Albums.ALBUM}, null, null,
                MediaStore.Audio.Albums.ALBUM + " ASC");

        Cursor mCursor =  getActivity().getApplicationContext().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                "0==0 ) GROUP BY (" + MediaStore.Audio.Media.ALBUM_ID,
                null,
                MediaStore.Audio.Media.ALBUM + " COLLATE NOCASE ASC");
//        Cursor mCursor2 = getActivity().getApplicationContext().getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, projection2, null, null, MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE ASC");

        if (cursor != null) {
            Log.d("TAG", "album:  " + cursor.getCount());
            cursor.moveToFirst();
            do{
                String albumId = cursor.getString(1);
                int  numberSong = Integer.parseInt(cursor.getString(2));
                String album = cursor.getString(3);
                Album temp = new Album(albumId, album, numberSong);
                listAlbum.add(temp);
            }while(cursor.moveToNext());
        }


        return listAlbum;
    }

    public ArrayList<Song> getListSongOfAlbum() {
        ArrayList<Song> listSong = new ArrayList<>();
        String id = mListAlbum.get(0).getmAlbumId();
//        String [] selectionArgs =  new String[] { "DISTINCT " + MediaStore.Audio.Playlists.Members.ALBUM_ID + " as _id"};
        String [] selectionArgs =  new String[] { "DISTINCT " + MediaStore.Audio.Playlists.Members.ALBUM_ID + " as " + id};
        Cursor cursor = getActivity().getContentResolver().query(
                MediaStore.Audio.Albums.getContentUri("external"),
                null, null, null,
                MediaStore.Audio.Albums.ALBUM + " ASC"
        );
        if( cursor != null) {
            cursor.moveToFirst();
            Log.d("TAG", "Size playlist album:  " + cursor.getCount());
            do {
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.DISPLAY_NAME));
                Log.d("TAG", "name:  " + title);
            }while( cursor.moveToNext());
        }

        return listSong;
    }

    @Override
    public void onItemAlbumSelect(int position) {
        ArrayList<Song> listSong = new ArrayList<>();
        listSong = mListAlbum.get(position).getmListSong();
        Intent intent = new Intent(this.getActivity(), DisplayListSongActivity.class);
        intent.putExtra(NAME_FRAGMENT, ALBUM_FRAGMENT);
        intent.putExtra(LIST_SONG_OF_ALBUM, listSong);
        startActivity(intent);
    }
}
