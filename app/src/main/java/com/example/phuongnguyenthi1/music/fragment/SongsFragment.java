package com.example.phuongnguyenthi1.music.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.MainActivity;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.activity.PlayMusicActivity;
import com.example.phuongnguyenthi1.music.adapter.SongAdapter;
import com.example.phuongnguyenthi1.music.model.Song;

import java.util.ArrayList;

import static com.example.phuongnguyenthi1.music.activity.PlayMusicActivity.POSITION_PLAY;

public class SongsFragment extends Fragment implements SongAdapter.OnClickItemSongListener{
    public static final String PATH_SONG = "PATH_SONG";
    ArrayList<Song> mListSongs = new ArrayList<>();
    RecyclerView mRcvSong;
    SongAdapter mSongAdapter ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_songs, container, false);

        mListSongs= (ArrayList<Song>) getArguments().getSerializable(MainActivity.LIST_SONG);

        mRcvSong = view.findViewById(R.id.fr_songs_rcv_songs);
        LinearLayoutManager mLayout = new LinearLayoutManager(getActivity());
        mLayout.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvSong.setLayoutManager(mLayout);

        mSongAdapter = new SongAdapter(this.getContext(), mListSongs, this);
        mRcvSong.setAdapter(mSongAdapter);
        return view;
    }

    public ArrayList<Song> getData() {
        ArrayList<Song> mData = new ArrayList<>();
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String projection[] ={
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID
        } ;
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cursor = cr.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        while( cursor.moveToNext()){
            int id = Integer.parseInt(cursor.getString(0));
            String artist = cursor.getString(1);
            String title  = cursor.getString(2);
            String data = cursor.getString(3);
            String album = cursor.getString(4);
            String albumId = cursor.getString(5);
            Song song = new Song(id, artist, title, data, album, albumId);

            mData.add(song);
        }

        return mData;
    }

    @Override
    public void onItemSongSelect(int position) {
        Intent intent = new Intent(getActivity(), PlayMusicActivity.class);
        intent.putExtra(PlayMusicActivity.LISTSONG_IN_PLAYMUSIC, mListSongs);
        intent.putExtra(POSITION_PLAY, position);
        startActivity(intent);

    }



}
