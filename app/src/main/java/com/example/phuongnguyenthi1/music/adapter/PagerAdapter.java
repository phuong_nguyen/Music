package com.example.phuongnguyenthi1.music.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class PagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<String> mListTitle = new ArrayList<>();
    ArrayList<Fragment> mListFragment = new ArrayList<>();
    Context mContext;



    public PagerAdapter(FragmentManager fm, Context mContext, ArrayList<Fragment> mListFragment, ArrayList<String> mListTitle) {
        super(fm);
        this.mListFragment = mListFragment;
        this.mContext = mContext;
        this.mListTitle = mListTitle;
    }

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mListFragment.get(position);
    }

    @Override
    public int getCount() {
        return mListFragment.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mListTitle.get(position);
    }
}
