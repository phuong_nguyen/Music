package com.example.phuongnguyenthi1.music.model;

import android.util.Log;

import java.io.Serializable;

public class Song implements Serializable {
    int ID;
    String mArtist, mTitle, mData, mAlbum, mAlbumId;

    public Song() {

    }

    public Song(int ID, String mArtist, String mTitle, String mData, String mAlbum, String mAlbumId) {
        this.ID = ID;
        this.mArtist = mArtist;
        this.mTitle = mTitle;
        this.mData = mData;
        this.mAlbum = mAlbum;
        this.mAlbumId = mAlbumId;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getmArtist() {
        return mArtist;
    }

    public void setmArtist(String mArtist) {
        this.mArtist = mArtist;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmData() {
        return mData;
    }

    public void setmData(String mData) {
        this.mData = mData;
    }

    public String getmAlbum() {
        return mAlbum;
    }

    public void setmAlbum(String mAlbum) {
        this.mAlbum = mAlbum;
    }

    public String getmAlbumId() {
        return mAlbumId;
    }

    public void setmAlbumId(String mAlbumId) {
        this.mAlbumId = mAlbumId;
    }

    public void displaySong(){
        Log.d("TAG", "id:  " + getID());
        Log.d("TAG", "artist:  " + getmArtist());
        Log.d("TAG", "title:  " + getmTitle());
        Log.d("TAG", "data:  " + getmData());
        Log.d("TAG", "album:  " + getmAlbum());
        Log.d("TAG", "------------------------------------------------" );
    }
}
