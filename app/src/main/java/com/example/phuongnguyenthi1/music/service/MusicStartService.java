package com.example.phuongnguyenthi1.music.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.activity.PlayMusicActivity;

public class MusicStartService extends Service {
    public static final String ACTION_PLAY_PAUSE = "com.example.phuongnguyenthi1.music.PLAY_PAUSE";
    public static final String ACTION_SEEK_TO = "com.example.phuongnguyenthi1.music.SEEK_TO";
    public static final String ACTION_GET_DURATION = "com.example.phuongnguyenthi1.music.GET_DURATION";
    public static final String ACTION_GET_POSTION = "com.example.phuongnguyenthi1.music.GET_POSITION";
    public static final String DURATION = "DURATION";
    MediaPlayer mPlayMusic;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("TAG", "on oncreate");
        mPlayMusic = MediaPlayer.create(this, R.raw.somethings);
        mPlayMusic.setLooping(true);
        mPlayMusic.setVolume(100, 100);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mPlayMusic.start();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PLAY_PAUSE);
        filter.addAction(ACTION_SEEK_TO);
        this.registerReceiver(receiver, filter);

        Intent intentGetDuration = new Intent();
        intentGetDuration.setAction(ACTION_GET_DURATION);
        intentGetDuration.putExtra(DURATION , getDuration());
        sendBroadcast(intentGetDuration);
        return START_STICKY;
    }

    private void playPause() {
        if( mPlayMusic.isPlaying()){
            mPlayMusic.pause();
        }else {
            mPlayMusic.start();
        }
    }

    private void seekTo(int position) {
        mPlayMusic.seekTo(position);
        mPlayMusic.start();
    }

    private void next(int position) {

    }

    private void back() {

    }

    private long getDuration() {
        return mPlayMusic.getDuration();
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch( action){
                case ACTION_PLAY_PAUSE:
                    playPause();
                    break;
                case ACTION_SEEK_TO:
                    int pos = intent.getExtras().getInt(PlayMusicActivity.POSITION);
                    seekTo(pos);
            }

        }
    };

}
