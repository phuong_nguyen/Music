package com.example.phuongnguyenthi1.music.model;

import android.content.ContentUris;
import android.net.Uri;

import java.util.ArrayList;

public class Artist {
    int mNumberSong;
    String mName;
    String mId;
    ArrayList<Song> mListSong  = new ArrayList<>();

    String mPathUri;

    public Artist(String name, ArrayList<Song> mListSong) {
        this.mName = name;
        this.mListSong = mListSong;

    }

    public Artist() {
    }

    public Artist(int mNumberSong, String name, String mId) {
        this.mNumberSong = mNumberSong;
        this.mName = name;
        this.mId = mId;
        Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri,
                Long.parseLong(this.mId));
    }

    public Artist(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public ArrayList<Song> getmListSong() {
        return mListSong;
    }

    public void setmListSong(ArrayList<Song> mListSong) {
        this.mListSong = mListSong;
    }

    public void addNewSong(Song song){
        mListSong.add(song);
    }

    public int getmNumberSong() {
        return mNumberSong;
    }

    public void setmNumberSong(int mNumberSong) {
        this.mNumberSong = mNumberSong;
    }

    public String getmPathUri() {
        return mPathUri;
    }
}
