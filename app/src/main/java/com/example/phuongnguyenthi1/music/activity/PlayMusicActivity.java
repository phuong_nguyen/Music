package com.example.phuongnguyenthi1.music.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phuongnguyenthi1.music.MainActivity;
import com.example.phuongnguyenthi1.music.R;
import com.example.phuongnguyenthi1.music.fragment.SongsFragment;
import com.example.phuongnguyenthi1.music.model.Song;
import com.example.phuongnguyenthi1.music.service.BackgroundService;
import com.example.phuongnguyenthi1.music.service.MusicBindService;
import com.example.phuongnguyenthi1.music.service.MusicStartService;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PlayMusicActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener{

    public static final String TAG = PlayMusicActivity.class.getName();

    public static final String POSITION = "POSITION";
    public static final String LISTSONG_IN_PLAYMUSIC = "LISTSONG_IN_PLAYMUSIC";
    public static final String POSITION_PLAY = "POSITION_PLAY";
    private static final int START_PROGRESS = 100;
    private static final int UPDATE_COUNT = 101;
    private SeekBar mSbPlay;
    private Button mBtnPrevious, mBtnPause, mBtnNext;
    private TextView mTvNameSong;
    private TextView mTvArtist;

    MusicBindService mBindService;
    final Handler mHandler = new Handler();

    Thread thread1;
    Handler mHandlerThread;
    String mPathSong;

    int currentPositionPlay;
    private ArrayList<Song> mListSong = new ArrayList<Song>();
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            mBindService = ((MusicBindService.MyBinder)iBinder).getService();
            mBindService.setPathSong(mPathSong);
            mBindService.initMediaPlayer();
            mBindService.setupMediaPlayer();

            mSbPlay.setMax((int)mBindService.getDuration() );
            mHandler.postDelayed(mUpdate, 0);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBindService = null;
        }
    };



    Runnable mUpdate = new Runnable() {

        @Override
        public void run() {
            if( mBindService.getDuration() != 0){
                mSbPlay.setMax((int)mBindService.getDuration());
            }
            mSbPlay.setProgress(mBindService.getCurrentPosition());
            mHandler.postDelayed(this, 100);
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);

        mListSong  = (ArrayList<Song>) getIntent().getSerializableExtra(LISTSONG_IN_PLAYMUSIC);

        currentPositionPlay = getIntent().getExtras().getInt(POSITION_PLAY);
        mPathSong = mListSong.get(currentPositionPlay).getmData();

        mTvNameSong = findViewById(R.id.acti_play_music_tv_name_song);
        mTvNameSong.setText(getTitleCurrentSong());
        mTvArtist = findViewById(R.id.acti_play_music_tv_artist);
        mTvArtist.setText(getArtist());
        mSbPlay = findViewById(R.id.sb_play_mmusic);
        mBtnNext = findViewById(R.id.btn_play_music_next);
        mBtnPause = findViewById(R.id.btn_play_music_pause);
        mBtnPrevious = findViewById(R.id.btn_play_music_previous);

        mSbPlay.setOnSeekBarChangeListener(this);
        mBtnPrevious.setOnClickListener(this);
        mBtnPause.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

//        startService with  MusicStartService
//        Intent intent = new Intent(this,MusicStartService.class);
//        startService(intent);
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(MusicStartService.ACTION_GET_DURATION);
//        filter.addAction(MusicStartService.ACTION_GET_POSTION);
//        this.registerReceiver(receiver, filter);

//        bindService with MusicBindService
        Intent intent = new Intent(PlayMusicActivity.this, MusicBindService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }



    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                  String action = intent.getAction();
                  switch( action){
                      case MusicStartService.ACTION_GET_DURATION:
                          long duration = intent.getExtras().getLong(MusicStartService.DURATION);
                          mSbPlay.setMax((int)duration);
                  }
        }
    };




    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_play_music_previous:
                change(-1);
                mTvNameSong.setText(getTitleCurrentSong());
                mTvArtist.setText(getArtist());
                break;
            case R.id.btn_play_music_pause:
                // start service
//                Intent intent = new Intent();
//                intent.setAction(MusicStartService.ACTION_PLAY_PAUSE);
//                sendBroadcast(intent);

                // bind service
                if (mBindService.isPlaying()) {
                    mBtnPause.setBackgroundResource(R.drawable.ic_pause);
                    mBindService.pause();
                    mHandler.removeCallbacks(mUpdate);
                } else {
                    mBtnPause.setBackgroundResource(R.drawable.ic_play);
                    mBindService.play();
                    mHandler.postDelayed(mUpdate, 100);
                }
//                mBindService.playPause();
                break;

            case R.id.btn_play_music_next:
                change(1);
                mTvNameSong.setText(getTitleCurrentSong());
                mTvArtist.setText(getArtist());
                break;
        }
    }

    private void change(int i) {
        mHandler.removeCallbacks(mUpdate);
        currentPositionPlay += i;
        if(currentPositionPlay < 0){
            currentPositionPlay = mListSong.size() -1;
        }
        if( currentPositionPlay == mListSong.size()){
            currentPositionPlay = 0;
        }
        mPathSong = mListSong.get(currentPositionPlay).getmData();
        mBindService.setPathSong(mPathSong);
        mBindService.initMediaPlayer();
        mBindService.setupMediaPlayer();

        mSbPlay.setMax((int)mBindService.getDuration() );
        mHandler.postDelayed(mUpdate, 0);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        //start service
//        Intent intent  = new Intent();
//        intent.putExtra(POSITION, i);
//        intent.setAction(MusicStartService.ACTION_SEEK_TO);
//        sendBroadcast(intent);

        // bind service

        seekBar.setProgress(i);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Toast.makeText(this, "positioin:  " + seekBar.getProgress(), Toast.LENGTH_SHORT).show();
        mBindService.seekTo(seekBar.getProgress());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public String getTitleCurrentSong() {
        return mListSong.get(currentPositionPlay).getmTitle();
    }

    public String getArtist() {
        return mListSong.get(currentPositionPlay).getmArtist();
    }
}
